# [hentapi](https://hentapi.com)

This is a open-source hentai api which fetches data from reddit and caches the posts into a database of post data and images to be retrieved later on.

## Prerequisites

- [rustup](https://rustup.rs) with the nightly toolchain installed

## Usage

At the moment, hentapi currently only downloads the first 27 posts of hentai from various subreddits. In the future, there will be a rest api along with it and a hentai finder website, hosted at [hentapi.com](https://hentapi.com). I also plan on implementing pagination.

You can run the downloader with `RUST_LOG=debug cargo run` after you have cloned this repository. The data will be stored in the **data** folder within the repository root.

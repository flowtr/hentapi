use anyhow::Result;
use hentapi::post::RedditPost;
use kv::*;

#[tokio::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();
    let cfg = Config::new("./data/db");
    let store = Store::new(cfg)?;
    let post_bucket = store.bucket::<&str, Json<RedditPost>>(Some("posts"))?;
    // TODO: rest api

    // if data/images does not exist, create it
    if !std::path::Path::new("./data/images").exists() {
        std::fs::create_dir("./data/images").unwrap();
    }

    let posts = hentapi::get_hentai().await.unwrap();

    // cache the posts with kv
    for post in posts {
        let id = post.id.clone();
        post_bucket
            .set(id.as_str(), Json(post))
            .expect("failed to store post");
    }

    Ok(())
}

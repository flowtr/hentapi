use std::str::FromStr;

use crate::post::RedditPost;
use anyhow::Result;
use hyper::{body, Body, Client, Response, Uri};
use hyper_tls::HttpsConnector;
use serde::{Deserialize, Serialize};

pub async fn read_response_body(res: Response<Body>) -> Result<hyper::body::Bytes, hyper::Error> {
    let bytes = body::to_bytes(res.into_body()).await?;
    Ok(bytes)
}

pub async fn read_response_string(res: Response<Body>) -> Result<String, hyper::Error> {
    let bytes = read_response_body(res).await?;
    Ok(String::from_utf8(bytes.to_vec()).unwrap())
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Subreddit {
    pub id: String,
    pub name: String,
    pub posts: Vec<RedditPost>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Listing<T> {
    pub after: Option<String>,
    pub before: Option<String>,
    pub children: Vec<T>,
    /// The number of posts in the listing.
    #[serde(rename = "dist")]
    pub total: u32,
    pub modhash: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ListingResponse<T> {
    pub data: Listing<T>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ListingData<T> {
    data: T,
}

impl Subreddit {
    pub async fn hot(name: &str) -> Result<Vec<RedditPost>> {
        let https = HttpsConnector::new();
        let client = Client::builder().build::<_, hyper::Body>(https);
        let url = Uri::from_str(format!("https://www.reddit.com/r/{}/hot.json", name).as_str())?;
        let res = client.get(url).await.unwrap();
        // get json from response body string
        let body = read_response_string(res).await?;
        let data: ListingResponse<ListingData<RedditPost>> = serde_json::from_str(&body)?;

        Ok(data.data.children.into_iter().map(|x| x.data).collect())
    }
}

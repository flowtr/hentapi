use std::{ffi::OsStr, path::Path, str::FromStr};

use crate::reddit::{read_response_body, Subreddit};
use anyhow::{Context, Result};
use hyper::{Client, Uri};
use hyper_tls::HttpsConnector;
use log::debug;
use post::RedditPost;

pub mod post;
pub mod reddit;

fn get_extension_from_filename(filename: &str) -> Option<&str> {
    Path::new(filename).extension().and_then(OsStr::to_str)
}

pub async fn get_hentai() -> Result<Vec<RedditPost>> {
    // collect all the hentai subreddit hot posts into a vector
    let (tx, mut rx) = tokio::sync::broadcast::channel::<RedditPost>(1);

    let subreddits = vec!["hentai", "ahegao"];
    for subreddit_name in subreddits {
        let tx = tx.clone();
        debug!("Fetching from r/{}", subreddit_name);
        let posts = Subreddit::hot(subreddit_name)
            .await
            .with_context(|| format!("Failed to fetch subreddit {}", subreddit_name))
            .unwrap();

        for post in posts {
            debug!(
                "Found post {} by {} at {}",
                post.id, post.author, post.permalink
            );

            // cache image url into a local file
            let image_url = post.url_overridden_by_dest.clone();
            if image_url.is_none() {
                continue;
            }

            let image_url = image_url.unwrap().clone();
            // get extension from image url path
            let extension = get_extension_from_filename(&image_url).unwrap_or("");
            let image_path = format!("data/images/{}.{}", post.id, extension);
            let id = post.id.clone();

            // check if image url contains gallery
            // if so, continue to next post as gallery urls are not supported
            if image_url.contains(&"gallery".to_string()) {
                continue;
            };

            // check if image already exists
            if !std::path::Path::new(&image_path).exists() {
                debug!("Downloading image {}", image_url);
                // get response or else log::error
                let https = HttpsConnector::new();
                let client = Client::builder().build::<_, hyper::Body>(https);
                let res = client
                    .get(Uri::from_str(&image_url).unwrap())
                    .await
                    .with_context(|| format!("Failed to get image for {}", id))
                    .unwrap();
                // get bytes or else log::error
                let bytes = read_response_body(res)
                    .await
                    .with_context(|| format!("Failed to get bytes for {}", id))
                    .unwrap();

                // write bytes to file
                tokio::fs::write(&image_path, bytes)
                    .await
                    .with_context(|| format!("Failed to write image for {}", id))
                    .unwrap();
            }

            tx.send(post).unwrap();
        }
    }

    let mut posts: Vec<RedditPost> = Vec::new();

    // collect all the posts from the channels
    loop {
        // if the channel is closed, break
        let post = rx.recv().await;
        if post.is_ok() {
            posts.push(post.unwrap());
        } else {
            break;
        }
    }

    Ok(posts)
}
